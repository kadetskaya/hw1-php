<?php

function sum($number) {
    if (gettype($number) !== 'integer') {
        return "$number - не является числом";
    }
    $num = abs($number);
    $sum = 0;
    while ($num > 0) {
        $sum = $sum + ($num % 10);
        $num = $num / 10;
    }
    return "Сумма цифр $number: $sum";
};

echo 'Посчитать сумму цифр заданного числа';
echo '<br>';
echo '<br>', sum(64);
echo '<br>', sum(1456);
echo '<br>', sum('five');

?>