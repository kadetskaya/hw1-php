<?php

function mirror ($number) {
    if (gettype($number) !== 'integer') {
        return "$number - не является числом";
    }
    $result = 0;
    while ($number) {
        $result *= 10;
        $result += $number % 10;
        $number = floor($number / 10);
    }
    return $result;
};

echo 'Вывести число, которое является зеркальным отображением последовательности цифр заданного числа';
echo '<br>';
echo '<br>', 'Зеркальное отображение 123: ', mirror (123);
echo '<br>', 'Зеркальное отображение 6789: ', mirror (6789);

?>
