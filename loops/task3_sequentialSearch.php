<?php

function squareRoot1 ($number) {
    if ($number === 0 || $number === 1) {
        return "Корень числа $number: $number";
    };
    if (gettype($number) !== "integer") {
        return "$number - не является числом";
    }
    $i = 1;
	while ($i * $i <= $number) {
        $i++;
    }
    $squareRoot = $i - 1;
	return "Корень числа $number с точностью до целого: $squareRoot";
};

echo 'Найти корень натурального числа с точностью до целого';
echo '<br>';
echo '<br>', squareRoot1('hello');
echo '<br>', squareRoot1(0);
echo '<br>', squareRoot1(1);
echo '<br>', squareRoot1(169);
echo '<br>', squareRoot1(65);

?>