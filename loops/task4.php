<?php

function factorial ($n) {
    $result = 1;
	$i = 1;
	while ($i <= $n) {
        $result = $result * $i;
        $i++;
    }
	return "Факториал числа $n = $result";
};

echo 'Вычислить факториал числа n. n! = 1*2*…*n-1*n;!';
echo '<br>';
echo '<br>', factorial (10);
echo '<br>', factorial (3);
echo '<br>', factorial (8);

?>