<?php

function isPrime ($number) {
    if ($number < 2) {
        return $number . ' - число должно быть больше 1.';
    } else if ($number == 2) {
        return $number . ' - простое число.';
    };
    for ($i = 2; $i <= round(sqrt($number)); $i++) {
        if ($number % $i == 0) {
            return $number . ' - составное число.';
        }
        return $number . ' - простое число.';
    }
};

echo 'Проверить простое ли число';
echo '<br>';
echo '<br>', isPrime (1);
echo '<br>', isPrime(2);
echo '<br>', isPrime(56);
echo '<br>', isPrime(23);

?>
