<?php

function squareRoot2($number) {
    if ($number == 0 || $number == 1) {
        return $number;
    };
    if (gettype($number) !== 'integer') {
        return $number . ' - не является числом';
    }
    $min = 0;
    $max = $number;
    while ($min <= $max) {
        $mid = floor(($min + $max) / 2);
    	if ($mid * $mid == $number) {
            return $mid;
        }
    	if ($mid * $mid < $number) {
            $min = $mid + 1;
        } else {
            $max = $mid - 1;
        }
    }
    return $max;
};

echo 'Найти корень натурального числа с точностью до целого';
echo '<br>';
echo '<br>', 'Корень числа 225: ', squareRoot2(225);
echo '<br>', 'Корень числа 81: ', squareRoot2(81);
echo '<br>', 'Корень числа 1: ', squareRoot2(1);
echo '<br>', 'Корень числа 100: ', squareRoot2(100);


?>