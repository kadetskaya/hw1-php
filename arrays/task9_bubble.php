<?php

function bubbleSort ($array) {
    if (is_array($array)) {
        for ($j = 0; $j < count($array) - 1; $j++){
            for ($i = 0; $i < count($array) - $j - 1; $i++){
                // если текущий элемент больше следующего
                if ($array[$i] > $array[$i + 1]){
                    // меняем местами элементы
                    $tempVar = $array[$i + 1];
                    $array[$i + 1] = $array[$i];
                    $array[$i] = $tempVar;
                }
            }
        }
        return $array;
    }
    return "$array - не является массивом";
};

echo 'Сортировка массива [3, 1, 4, 2] пузырьком: ', json_encode(bubbleSort([3, 1, 4, 2]));
echo '<br>', "Сортировка массива [2, 4, 98, 67, -59, 6, 345, -9, 115] пузырьком: ", json_encode(bubbleSort([2, 4, 98, 67, -59, 6, 345, -9, 115]));
echo '<br>', "Сортировка массива 5677 пузырьком: ", bubbleSort(5677);

?>
