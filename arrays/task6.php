<?php

function reverseArr ($array) {
    if (is_array($array)) {
        $newArr = array_reverse($array);
        return $newArr;
    }
    return "$array - не является массивом";
};

echo 'Реверс массива ([2, 4, 98, 67, -59, 6, 345, -9, 115]): ', json_encode(reverseArr ([2, 4, 98, 67, -59, 6, 345, -9, 115]));
echo '<br>', 'Реверс массива (6789): ', reverseArr (6789);

?>
