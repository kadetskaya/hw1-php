<?php

function maxIndex ($array) {
    if (is_array($array)) {
        $maxIndex = array_keys($array, max($array))[0];
        return "Индекс максимального элемента массива: $maxIndex";
    }
    return "$array - не является массивом";
};

echo 'Найти индекс максимального элемента массива';
echo '<br>';
echo '<br>', maxIndex([2, 4, 98, 67, -59, 6, 345, -9, 115]);
echo '<br>', maxIndex (67);

?>