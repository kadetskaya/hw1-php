<?php

function insertionSort ($array) {
    if (is_array($array)) {
        for ($i = 0; $i < count($array); $i++) {
            $tempVar = $array[$i];
            $j = $i - 1;
            while ($j >= 0 && $array[$j] > $tempVar) {
                $array[$j + 1] = $array[$j];
                $j--;
            }
            $array[$j + 1] = $tempVar;
        }
        return $array;
    }
    return "$array - не является массивом";
};

echo 'Сортировка массива [3, 1, 4, 2] вставками: ', json_encode(insertionSort([3, 1, 4, 2]));
echo '<br>', 'Сортировка массива [2, 4, 98, 67, -59, 6, 345, -9, 115] вставками: ', json_encode(insertionSort([2, 4, 98, 67, -59, 6, 345, -9, 115]));
echo '<br>', 'Сортировка массива 5677 вставками: ', insertionSort(5677);

?>