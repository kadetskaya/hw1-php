<?php

function selectSort ($array) {
    if (is_array($array)) {
        $count = count($array);
        if ($count <= 1) {
            return $array;
        }
        for ($i = 0; $i < $count; $i++) {
            $k = $i;

            for ($j = $i + 1; $j < $count; $j++) {
                if ($array[$k] > $array[$j]) {
                    $k = $j;
                }

                if ($k != $i) {
                    $tempVar = $array[$i];
                    $array[$i] = $array[$k];
                    $array[$k] = $tempVar;
                }
            }
        }
        return $array;
    }
    return "$array - не является массивом";
};

echo 'Сортировка массива [3, 1, 4, 2] выбором: ', json_encode(selectSort([3, 1, 4, 2]));
echo '<br>', "Сортировка массива [2, 4, 98, 67, -59, 6, 345, -9, 115] выбором: ", json_encode(selectSort([2, 4, 98, 67, -59, 6, 345, -9, 115]));
echo '<br>', "Сортировка массива 5677 выбором: ", selectSort(5677);

?>