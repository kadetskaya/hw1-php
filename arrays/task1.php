<?php

function minEl ($array) {
    if (is_array($array)) {
        $min = $array[0];
        for ($i = 1; $i < count($array); $i++) {
            if ($array[$i] < $min) {
                $min = $array[$i];
            }
        }
        return "Минимальный элемент массива: $min";
    }
    return "$array - не является массивом";
};

echo 'Найти минимальный элемент массива';
echo '<br>';
echo '<br>', minEl([2, 4, 98, 67, -59, 6, 345, -9, 115]) ;
echo '<br>', minEl(123) ;

?>