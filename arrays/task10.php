<?php

function sum($number) {
    if (gettype($number) !== "integer") {
        return "$number - не является числом";
    }
    $number = abs($number);
    $sum = 0;
    while ($number > 0) {
        $sum = $sum + ($number % 10);
        $number = $number / 10;
    }
    return "Сумма цифр заданого числа: $sum";
};

echo sum(64);
echo '<br>', sum(1456);
echo '<br>', sum('five');

?>