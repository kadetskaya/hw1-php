<?php

function maxEl ($array) {
    if (is_array($array)) {
        $max = $array[0];
		for ($i = 1; $i < count($array); $i++) {
            if ($array[$i] > $max) {
                $max = $array[$i];
            }
        }
		return "Максимальный элемент массива: $max";
	}
	return "$array -  не является массивом";
};

echo 'Найти максимальный элемент массива';
echo '<br>';
echo '<br>', maxEl([2, 4, 98, 67, -59, 6, 345, -9, 115]) ;
echo '<br>', maxEl(321) ;

?>
