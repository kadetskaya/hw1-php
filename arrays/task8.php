<?php

function halfReverse ($array) {
    if (is_array($array)) {
        $mid = count($array) / 2;
		for ($i = 0; $i < $mid; $i++) {
            $part1 = $array[$i];
			$array[$i] = $array[$i + $mid];
			$array[$i + $mid] = $part1;
		}
		return $array;
	}
    return "$array - не является массивом";
}

echo 'Поменять местами первую и вторую половину массива';
echo '<br>';
echo '<br>', '[1, 2, 3, 4]: ', json_encode(halfReverse([1, 2, 3, 4]));
echo '<br>', '[5, 6, 7, 8]: ', json_encode(halfReverse([5, 6, 7, 8]));
echo '<br>', halfReverse(7890);

?>