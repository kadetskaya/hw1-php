<?php

function sum ($array) {
    if (is_array($array)) {
        $sum = 0;
		for ($i = 0; $i < count($array); $i++) {
            if ($i % 2 != 0)
                $sum += $array[$i];
        }
		return "Сумма элементов с нечетными индексами: $sum";
	}
	return "$array - не является массивом";
};

echo 'Посчитать сумму элементов массива с нечетными индексами';
echo '<br>';
echo '<br>', sum([2, 4, 98, 67, -59, 6, 345, -9, 115]);
echo '<br>', sum(1456);
echo '<br>', sum('five');

?>