<?php

function amount ($array) {
    if (is_array($array)) {
        $counter = 0;
		for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] % 2 != 0) {
                $counter++;
            }
        }
		return "Количество нечетных элементов массива: $counter.";
	}
	return "$array - не является массивом";
};

echo 'Посчитать количество нечетных элементов массива';
echo '<br>';
echo '<br>', amount ([2, 4, 98, 67, -59, 6, 345, -9, 115]);
echo '<br>', amount (468);
echo '<br>', amount ('six');

?>