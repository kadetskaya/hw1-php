<?php

function minIndex ($array) {
    if (is_array($array)) {
        $minIndex = array_keys($array, min($array))[0];
        return "Индекс минимального элемента массива: $minIndex";
    }
    return "$array - не является массивом";
};

echo 'Найти индекс минимального элемента массива';
echo '<br>';
echo '<br>', minIndex([2, 4, 98, 67, -59, 6, 345, -9, 115]);
echo '<br>', minIndex( 567);

?>