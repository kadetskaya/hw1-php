<?php

function maxValue ($a, $b, $c) {
    if ($a*$b*$c > $a+$b+$c) {
        return $a*$b*$c+3;
    } else if ($a*$b*$c < $a+$b+$c) {
        return $a+$b+$c+3;
    } else if ($a*$b*$c === $a+$b+$c) {
        return $a+$b+$c+3;
    }
};

echo 'Посчитать выражение макс(а*б*с, а+б+с)+3';
echo '<br>';
echo '<br>', "Сумма выражения макс (2, 4, 6)+3: ", maxValue (2, 4, 6);
echo '<br>', "Сумма выражения макс (1, 2, 3)+3: ", maxValue (1, 2, 3);

?>
