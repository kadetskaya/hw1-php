<?php

function whichQuarter ($x, $y) {
	if ($x === 0 && $y === 0) {
		return "Точка ($x, $y) находится в начале координат.";
	} else if ($x === 0) {
		return "Точка находится на оси 'x'.";
	} else if ($y == 0) {
		return "Точка ($x, $y) находится на оси 'y'.";
	} else if ($x > 0) {
		if ($y > 0) {
			return "Точка ($x, $y) находится в I четверти.";
		} else if ($y < 0) {
			return "Точка ($x, $y) находится в IV четверти.";
		}
	} else if ($x < 0) {
		if ($y > 0) {
			return "Точка ($x, $y) находится в II четверти.";
		} else if ($y < 0) {
			return "Точка ($x, $y) находится в III четверти.";
		}
	}
};

echo 'Определить какой четверти принадлежит точка с координатами (х,у)';
echo '<br>';
echo '<br>', whichQuarter (-95, 16);
echo '<br>', whichQuarter(23, -14);
echo '<br>', whichQuarter(13,13);
echo '<br>', whichQuarter(-30, -26);
echo '<br>', whichQuarter(0, 0);

?>
