<?php

function mark ($score) {
    if ($score < 0 || $score > 100) {
        return "score is out of range.";
    }
    if ($score < 20) {
        return "F";
    }
    if ($score < 40) {
        return "E";
    }
    if ($score < 60) {
        return "D";
    }
    if ($score < 75) {
        return "C";
    }
    if ($score < 90) {
        return "B";
    }
    if ($score <= 100) {
        return "A";
    }
};

echo 'Написать программу определения оценки студента по его рейтингу';
echo '<br>';
echo '<br>', "Score 155: ", mark (155);
echo '<br>', "Score 15: ", mark (15);
echo '<br>', "Score 26: ", mark (26);
echo '<br>', "Score 51: ", mark (51);
echo '<br>', "Score 69: ", mark (69);
echo '<br>', "Score 81: ", mark (81);
echo '<br>', "Score 97: ", mark (97);

?>
