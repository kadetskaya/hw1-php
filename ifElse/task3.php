<?php

function sum ($a, $b, $c) {
	$result = 0;
	if ($a > 0) {
		$result += $a;
	}
	if ($b > 0) {
		$result += $b;
	}
	if ($c > 0) {
		$result += $c;
	}
	if ($result > 0) {
		return $result;
	}
	return "все числа отрицательные.";
};

echo 'Найти суммы только положительных из трех чисел';
echo '<br>';
echo '<br>', "Сумма положительных чисел  (10, 125, -45): ", sum(10, 125, -45);
echo '<br>', "Сумма положительных чисел  (-4, -35, -45): ", sum(-4, -35, -45);
echo '<br>', "Сумма положительных чисел  (-4, 98, 1): ", sum(-4, 98, 1);

?>
