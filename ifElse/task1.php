<?php

function c($a, $b) {
    if ($a % 2 === 0) {
        $result = $a * $b;
        return $result;
    } else
        $result = $a + $b;
    return $result;
};

echo 'Если а – четное посчитать а*б, иначе а+б';
echo '<br>';
echo '<br>', 'a = 5, b = 4; result = ', c(5, 4);
echo '<br>', 'a = 10, b = 6; result = ', c(10, 6);

?>
