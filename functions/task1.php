<?php

function getDay($number) {
    $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    if ($number < 1 || $number > count($days)) {
        return "Номер дня введен некорректно. Попробуйте число от 1 до 7.";
    } else {
        $day = $days[$number - 1];
        return "День №$number: $day";
    }
};

echo 'Получить строковое название дня недели по номеру дня';
echo '<br>';
echo '<br>', getDay(1);
echo '<br>', getDay(5);
echo '<br>', getDay(7);

?>