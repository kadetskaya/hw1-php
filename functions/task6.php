<?php

$numbers2 = array (
    'zero'=> 0,
	'one'=> 1,
	'two'=> 2,
	'three'=> 3,
	'four'=> 4,
	'five'=> 5,
	'six'=> 6,
	'seven'=> 7,
	'eight'=> 8,
	'nine'=> 9,
	'ten'=> 10,
	'eleven'=> 11,
	'twelve'=> 12,
	'thirteen'=> 13,
	'fourteen'=> 14,
	'fifteen'=> 15,
	'sixteen'=> 16,
	'seventeen'=> 17,
	'eighteen'=> 18,
	'nineteen'=> 19,
	'twenty'=> 20,
	'thirty'=> 30,
	'forty'=> 40,
	'fifty'=> 50,
	'sixty'=> 60,
	'seventy'=> 70,
	'eighty'=> 80,
	'ninety'=> 90
);


function convert_tens3 ($num) {
    global $numbers2;
    $numArr = explode(" ", $num);
    $sum = 0;
    for ($i = 0; $i < count($numArr); $i++) {
        $sum += $numbers2[$numArr[$i]];
    }
    return $sum;
};

function convert_hundreds3 ($num) {
    $a = array ();
    $b = array ();
    $numArr = explode(" ", $num);
    for ($i = 0; $i < count($numArr); $i++) {
        if ($numArr[$i] == "hundred") {
            $a = array_slice($numArr, 0, $i);
            if ($i != count($numArr) - 1) {
                $b = array_slice($numArr, $i+1);
                return convert_tens3(implode(" ", $a)) * 100 + convert_tens3(implode(" ", $b));
            }
            return convert_tens3(implode(" ", $a)) * 100;
        }
    }
    return convert_tens3($num);
};

function convert_thousands3 ($num) {
    $numArr = explode(" ", $num);
    for ($i = 0; $i < count($numArr); $i++) {
        if ($numArr[$i] == "thousand") {
            $a = array_slice($numArr, 0, $i);
            if ($i != count($numArr) - 1) {
                $b = array_slice($numArr, $i+1);
                return convert_hundreds3(implode(" ", $a)) * 1000 + convert_hundreds3(implode(" ", $b));
            }
            return convert_hundreds3(implode(" ", $a)) * 1000;
        }
    }
    return convert_hundreds3($num);
};

function convert_millions3 ($num) {
    $numArr = explode(" ", $num);
    for ($i = 0; $i < count($numArr); $i++) {
        if ($numArr[$i] == "million") {
            $a = array_slice($numArr, 0, $i);
            if ($i != count($numArr) - 1) {
                $b = array_slice($numArr, $i+1);
                return convert_hundreds3(implode(" ", $a)) * 1000000 + convert_thousands3(implode(" ", $b));
            }
            return convert_hundreds3(implode(" ", $a)) * 1000000;
        }
    }
    return convert_thousands3($num);
};

function convert_billions3 ($num) {
    $numArr = explode(" ", $num);
    for ($i = 0; $i < count($numArr); $i++) {
        if ($numArr[$i] == "minus") {
            return "the number is not in the specified range.";
        }
        if ($numArr[$i] == "billion") {
            $a = array_slice($numArr, 0, array_search("billion", $numArr));
            if ($i != count($numArr) - 1) {
                $b = array_slice($numArr, (array_search("billion", $numArr) + 1));
                return convert_hundreds3(implode(" ", $a)) * 1000000000 + convert_millions3(implode(" ", $b));
            }
            return convert_hundreds3(implode(" ", $a)) * 1000000000;
        }
    }
    return convert_millions3($num);
};

function convertToNumbers2 ($num) {
    $numArr = explode(" ", $num);
    if (count($numArr) > 19) {
        return "the number is not in the specified range.";
    }
    return convert_billions3($num);
};

echo '**Для задания 3 расширить диапазон до 999 миллиардов';
echo '<br>';
echo '<br>', 'zero - ', convertToNumbers2('zero');
echo '<br>', 'one hundred thousand fifty five - ', convertToNumbers2('one hundred thousand fifty five');
echo '<br>', 'minus one hundred thousand fifty five - ', convertToNumbers2('minus one hundred thousand fifty five');
echo '<br>', 'one million one hundred thousand fifty five - ', convertToNumbers2('one million one hundred thousand fifty five');
echo '<br>', 'five hundred sixty nine billion one million one hundred thousand fifty five - ', convertToNumbers2('five hundred sixty nine billion one million one hundred thousand fifty five');
echo '<br>', 'nine hundred ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine - ',
              convertToNumbers2('nine hundred ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million nine hundred ninety nine thousand nine hundred ninety nine');
echo '<br>', 'five hundred thousand sixty seven - ', convertToNumbers2('five hundred thousand sixty seven');
echo '<br>', 'eight hundred fifty five - ', convertToNumbers2('eight hundred fifty five');
echo '<br>', 'twenty four - ', convertToNumbers2('twenty four');
echo '<br>', 'two hundred ninety nine thousand five hundred seventeen - ', convertToNumbers2('two hundred ninety nine thousand five hundred seventeen');

?>
