<?php

$ones2 = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
$tens2 = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
$teens2 = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];


function convert_tens2 ($num) {
    global $ones2, $teens2, $tens2;
    if ($num < 10) {
        return $ones2[$num];
    } else if ($num >= 10 && $num < 20) {
        return $teens2[$num - 10];
    }
    else {
        return $tens2[intval(floor($num / 10))] . " " . $ones2[$num % 10];
    }
};

function convert_hundreds2 ($num) {
    global $ones2;
    if ($num > 99) {
        return $ones2[intval(floor($num / 100))] . " hundred " . convert_tens2($num % 100);
    } else {
        return convert_tens2($num);
    }
};

function convert_thousands2 ($num) {
    if ($num >= 1000) {
        return convert_hundreds2(intval(floor($num / 1000))) . " thousand " . convert_hundreds2($num % 1000);
    } else {
        return convert_hundreds2($num);
    }
};

function convert_millions2 ($num) {
    if ($num >= 1000000) {
        return convert_hundreds2(intval(floor($num / 1000000))) . " million " . convert_thousands2($num % 1000000);
    } else {
        return convert_thousands2($num);
    }
};

function convert_billions2 ($num) {
    if ($num >= 1000000000) {
        return convert_hundreds2(intval(floor($num / 1000000000))) . " billion " . convert_millions2($num % 1000000000);
    } else {
        return convert_millions2($num);
    }
};

function convertToLetters2 ($num) {
    if ($num == 0) {
        return "zero";
    }
    if ($num < 0 || $num > 999999999999) {
        return "The number is not in the specified range.";
    } else {
        return convert_billions2($num);
    }
};

echo '**Для задания 2 расширить диапазон до 999 миллиардов';
echo '<br>';
echo '<br>', '78 - ', convertToLetters2(78);
echo '<br>', '1023 - ', convertToLetters2(1023);
echo '<br>', '0 - ', convertToLetters2(0);
echo '<br>', '-10 -  ', convertToLetters2(-10);
echo '<br>', '23 -  ', convertToLetters2(23);
echo '<br>', '563 - ', convertToLetters2(563);
echo '<br>', '6423 - ', convertToLetters2(6423);
echo '<br>', '5423000 - ', convertToLetters2(5423000);
echo '<br>', '999999000999 - ', convertToLetters2(999999000999);
echo '<br>', '1000000000000 - ', convertToLetters2(1000000000000);

?>