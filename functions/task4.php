<?php

function getDistance ($x1, $y1, $x2, $y2) {
    $i = pow(($x2 - $x1), 2);
	$j = pow(($y2 - $y1), 2);
	$result = (sqrt($i + $j));
	return number_format($result, 2);
};

echo 'Найти расстояние между двумя точками в двумерном декартовом пространстве';
echo '<br>';
echo '<br>', 'Расстояние между двумя точками: ', getDistance(0, 0, 1, 1);
echo '<br>', 'Расстояние между двумя точками: ', getDistance(0, 0, 0, 0);
echo '<br>', 'Расстояние между двумя точками: ', getDistance(0, 0, -8, -15);
echo '<br>', 'Расстояние между двумя точками: ', getDistance(1, 1, 1, 2);

?>