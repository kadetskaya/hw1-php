<?php

$ones = array ('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять');
$tens = array ('', '', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
$teens = array ('десять', 'одинадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
$hundreds = array ('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');


function convert_tens ($num) {
    global $ones, $tens, $teens;
    if ($num < 10) {
        return $ones[$num];
    } else if ($num >= 10 && $num < 20) {
        return $teens[$num - 10];
    } else {
        $index = intval(floor($num / 10));
        return $tens[$index] . " " . $ones[$num % 10];
    }
};

function convert_hundreds ($num) {
    global $hundreds;
    if ($num > 99) {
        $index = intval(floor($num / 100));
        return $hundreds[$index] . " " . convert_tens($num % 100);
    } else {
        return convert_tens($num);
    }
};

function convertToLetters ($num) {
    if ($num == 0) {
        return "ноль";
    }
    if ($num < 0 || $num > 999) {
        return "число не входит в заданный диапазон 0-999.";
    }
    else return convert_hundreds($num);
};

echo 'Вводим число (0-999), получаем строку с прописью числа';
echo '<br>';
echo '<br>', '78 - ', convertToLetters(78);
echo '<br>', "1023 - ", convertToLetters(1023);
echo '<br>', "0 - ", convertToLetters(0);
echo '<br>', "-10 -  ", convertToLetters(-10);
echo '<br>', "23 -  ", convertToLetters(23);
echo '<br>', "563 - ", convertToLetters(563);

?>
